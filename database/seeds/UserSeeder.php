<?php
use App\User;
use Illuminate\Database\Seeder;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	// $faker = Faker\Factory::create();

      

       
            User::create([  
            	'id' => 2,
                'name' => 'john',  
                'email' => 'john@email.com',  
                'password' => Hash::make('doe'),
                'created_at'=> date('Y-m-d G:i:s'),
                'updated_at'=> date('Y-m-d G:i:s')
        
            ]);
          

    }
}
