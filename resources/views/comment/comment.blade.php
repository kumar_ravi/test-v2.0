
@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">All comments</div>
				<div class="panel-body">
					
					<table id="table" class="display" cellspacing="0" width="100%">
        
      
        <tbody>
           
               @if (isset($comments))               
    				@foreach($comments as $data)

                 <tr><td>{{$data->comment}}</td></tr>

                 
                

               	 	@endforeach
                @endif
                
            

            </tbody>
            </table>
            <div class="col-md-2"></div>
            @if(\Auth::user())
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/comments/add') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
						<input type="hidden" name="hotelid" value="{{ $hotelid }}">
							<div class="col-md-6 center-block">
								<textarea type="text" rows="2" cols="20" class="form-control center-block" name="comment" value="{{ old('comment') }}">  </textarea>
							</div>
						</div>

						


						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Post Comment
								</button>
							</div>
						</div>
					</form>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
