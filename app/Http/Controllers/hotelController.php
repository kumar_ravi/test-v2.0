<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;


use Illuminate\Http\Request;
use DB;
use Validator;
use App\hotel;

class hotelController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
			
		if(Auth::user()->id == 1){
			return view("hotel/hotel");

		}
		else{
			return view("welcome");
		}
	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		
		 $rules = array('name' => 'required'
        );

        $messages = array('required' => 'The :attribute is required.');

        $validator = \Validator::make(\Input::all(), $rules, $messages);

        if ($validator -> fails()) {

            // get the error messages from the validator
            $messages = $validator -> messages();

       
            return \Redirect::to('/admin') -> withErrors($validator);

        } else {
            

            
            $name = $request -> input('name');

            $hotel = new hotel;  // new instance of hotel table
           
            $hotel -> name = $name;
            $hotel -> created_at = date('Y-m-d G:i:s');
            $hotel -> updated_at = date('Y-m-d G:i:s');

            $hotel -> save();

            return \Redirect::to('/');
        }

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
