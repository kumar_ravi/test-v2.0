<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\comment;

use Illuminate\Http\Request;

class commentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($hotelId)
	{
		 $comments = comment::where('hotel_id',$hotelId)->get();
		 return view('comment/comment')->with('comments',$comments)->with('hotelid',$hotelId);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		 
		 $hotelid = $request -> input('hotelid');
		 $rules = array('comment' => 'required'
        );

        $messages = array('required' => 'The :attribute is required.');

        $validator = \Validator::make(\Input::all(), $rules, $messages);

        if ($validator -> fails()) {

            // get the error messages from the validator
            $messages = $validator -> messages();

       
            return \Redirect::to("/viewcomment/$hotelid") -> withErrors($validator);

        } else {
            

            
            $commentData = $request -> input('comment');
         

            $comment = new comment;  // new instance of hotel table
            $comment -> hotel_id = $hotelid;
            $comment -> comment = $commentData;
            $comment -> created_at = date('Y-m-d G:i:s');
            $comment -> updated_at = date('Y-m-d G:i:s');

            $comment -> save();

            return \Redirect::to("/viewcomment/$hotelid");
        }

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
