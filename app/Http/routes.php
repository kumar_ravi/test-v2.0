<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/viewcomment/{hotelId}', 'commentController@index');
Route::get('/admin', 'hotelController@index');                          // this will call controller in hotel controller
Route::post('/hotel/register','hotelController@create');     
Route::post('/comments/add','commentController@create');            // calling function which will create new hotel
Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
