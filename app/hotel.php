<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class hotel extends Model {

	//
	public function commentsOfHotel(){
        return $this->hasMany('\App\comment','id','hotel_id'); // one hotel may have many comments
    }

}
