<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model {

	public function hotelOfComment(){
        return $this->belongTo('\App\hotel','hotel_id');  // coment must belong to a hotel
    }

}
